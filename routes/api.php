<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

/*------------------------------------ URL Modules Reservation Train -----------------------------------------*/
Route::apiResource('train', 'ReservationTrain\TrainController');
Route::apiResource('voyageur', 'ReservationTrain\VoyageurController');
Route::apiResource('places', 'ReservationTrain\PlacesController');
Route::apiResource('reservation', 'ReservationTrain\ReservationController');
Route::apiResource('itineraire', 'ReservationTrain\ItineraireController');

// Specifique
Route::get('itineraire/{id}/reservations', 'ReservationTrain\ItineraireController@itineraireReserver');
Route::get('train/{id}/places', 'ReservationTrain\TrainController@trainPlace');
Route::get('train/{id}/places/disponible', 'ReservationTrain\TrainController@trainPlaceDisponible');
Route::get('train/{id}/places/{idplace}', 'ReservationTrain\TrainController@trainPlaceReserver');
Route::get('train/{id}/places/Occupe', 'ReservationTrain\TrainController@trainPlaceOccupe');
Route::get('train/{id}/recette/annuel', 'ReservationTrain\TrainController@trainRecetteAnnuel');
Route::get('train/{id}/recette/mensuel', 'ReservationTrain\TrainController@trainRecetteMensuel');


/*------------------------------------ URL Modules Bancaire -----------------------------------------*/
Route::apiResource('banque/client', 'Bancaire\ClientController');
Route::apiResource('banque/retrait', 'Bancaire\RetraitController');
Route::apiResource('banque/versement', 'Bancaire\VersementController');

// Specifique
Route::get('banque/clients', 'Bancaire\ClientController@rechercheClient');
Route::get('banque/clients/effectif', 'Bancaire\ClientController@effectifClient');
Route::get('banque/clients/{id}/versements', 'Bancaire\ClientController@versementClient');
Route::get('banque/clients/{id}/retraits', 'Bancaire\ClientController@retraitClient');
Route::get('banque/clients/{id}/totalTransaction', 'Bancaire\ClientController@totalTransactionClient');


/*------------------------------------ URL Modules Commande -----------------------------------------*/
Route::apiResource('commande/client', 'Commande\ClientController');
Route::apiResource('commande/approvisionnement', 'Commande\ApprovisionnementController');
Route::apiResource('commande/fournisseur', 'Commande\FournisseurController');
Route::apiResource('commande/produit', 'Commande\ProduitController');
Route::apiResource('commande/vente', 'Commande\VenteController');

// Specifique
Route::get('commande/clients', 'Commande\ClientController@rechercheClients');
Route::get('commande/clients/{id}/produits', 'Commande\ClientController@venteProduitParClient');
Route::get('commande/clients/{id}/produits/totalMontant', 'Commande\ClientController@venteProduitParClientTotal');
Route::get('commande/fournisseurs/{id}/produits', 'Commande\FournisseurController@approProduitParFournisseur');
Route::get('commande/fournisseurs/{id}/produits/totalMontant', 'Commande\FournisseurController@approProduitParFournisseurTotal');
Route::get('commande/produits', 'Commande\ProduitController@rechercheProduits');
Route::get('commande/produits/{id}/mouvement/vente/annuel', 'Commande\ProduitController@mouvementVentAnnuel');
Route::get('commande/produits/{id}/mouvement/vente/mensuel', 'Commande\ProduitController@mouvementVentMensuel');
Route::get('commande/produits/{id}/mouvement/vente', 'Commande\ProduitController@mouvementVent2Date');
Route::get('commande/produits/{id}/mouvement/approvisionnement/annuel', 'Commande\ProduitController@mouvementApprovisionnementAnnuel');
Route::get('commande/produits/{id}/mouvement/approvisionnement/mensuel', 'Commande\ProduitController@mouvementApprovisionnementMensuel');
Route::get('commande/produits/{id}/mouvement/approvisionnement', 'Commande\ProduitController@mouvementApprovisionnement2Date');
Route::get('commande/produits/{id}/mouvement/total/annuel', 'Commande\ProduitController@mouvementTotalAnnuel');
Route::get('commande/produits/{id}/mouvement/total/mensuel', 'Commande\ProduitController@mouvementTotalMensuel');
Route::get('commande/produits/{id}/mouvement/total', 'Commande\ProduitController@mouvementTotal');


/*------------------------------------ URL Modules Employe -----------------------------------------*/
Route::apiResource('employee/employe', 'Employee\EmployeController');
Route::apiResource('employee/entreprise', 'Employee\EntrepriseController');
Route::apiResource('employee/travail', 'Employee\TravailController');

// Specifique
Route::get('employee/entreprises/{id}/employes', 'Employee\EntrepriseController@listeEmploye');
Route::get('employee/entreprises/{id}/nombre_employes', 'Employee\EntrepriseController@nombreEmployes');
Route::get('employee/employes/salaires', 'Employee\EmployeController@salaireGlobauxEmploye');
Route::get('employee/employes/montant', 'Employee\EmployeController@montantTotalEmploye');
Route::get('employee/employes/disponible', 'Employee\EmployeController@employeDisponible');
