<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetraitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retraits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_retrait');
            $table->integer('client_banque_id');
            $table->foreign('client_banque_id')->references('id')->on('client_banques')->onUpdate('cascade');
            $table->float('montant');
            $table->string('numero_cheque');
            $table->date('date_retrait');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retraits');
    }
}
