<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVersementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('versements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_versement');
            $table->integer('client_banque_id');
            $table->foreign('client_banque_id')->references('id')->on('client_banques')->onUpdate('cascade');
            $table->float('montant');
            $table->date('date_versement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('versements');
    }
}
