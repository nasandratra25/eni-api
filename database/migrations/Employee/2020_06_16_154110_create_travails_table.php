<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('employe_id');
            $table->foreign('employe_id')->references('id')->on('employes')->onUpdate('cascade');
            $table->integer('entreprise_id');
            $table->foreign('entreprise_id')->references('id')->on('entreprises')->onUpdate('cascade');
            $table->float('nombre_heure');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travails');
    }
}
