<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_reservation',255);
            $table->integer('train_id');
            $table->foreign('train_id')->references('id')->on('trains')->onUpdate('cascade');
            $table->integer('place_id');
            $table->foreign('place_id')->references('id')->on('places')->onUpdate('cascade');
            $table->integer('voyageur_id');
            $table->foreign('voyageur_id')->references('id')->on('voyageurs')->onUpdate('cascade');
            $table->date('date_reservation')->default(date('Y-M-d'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
