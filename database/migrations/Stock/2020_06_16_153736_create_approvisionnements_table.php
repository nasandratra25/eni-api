<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApprovisionnementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approvisionnements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_approvisionnement');
            $table->integer('produit_id');
            $table->foreign('produit_id')->references('id')->on('produits')->onUpdate('cascade');
            $table->integer('fournisseur_id');
            $table->foreign('fournisseur_id')->references('id')->on('fournisseurs')->onUpdate('cascade');
            $table->integer('quantite');
            $table->date('date_approvisionnement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approvisionnements');
    }
}
