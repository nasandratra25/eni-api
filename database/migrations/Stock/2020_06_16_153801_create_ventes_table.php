<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_vente');
            $table->integer('produit_id');
            $table->foreign('produit_id')->references('id')->on('produits')->onUpdate('cascade');
            $table->integer('client_commande_id');
            $table->foreign('client_commande_id')->references('id')->on('client_commandes')->onUpdate('cascade');
            $table->integer('quantite');
            $table->date('date_vente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventes');
    }
}
