<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;

class Entreprise extends Model
{
    protected $table = 'entreprises';
    public $timestamps = true;
    protected $fillable = [
        'numero_entreprise',
        'nom',
        'raison_sociale',

    ];
}
