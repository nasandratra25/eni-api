<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;

class Travail extends Model
{
    protected $table = 'travails';
    public $timestamps = true;
    protected $fillable = [
        'employe_id',
        'entreprise_id',
        'nombre_heure',
    ];
}
