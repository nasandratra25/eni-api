<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;

class Employe extends Model
{
    protected $table = 'employes';
    public $timestamps = true;
    protected $fillable = [
        'numero_employe',
        'nom',
        'prenom',
        'adresse',
        'cin',
        'contact',
        'taux_horaire',
        'disponibilite',
    ];
}
