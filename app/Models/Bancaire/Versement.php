<?php

namespace App\Models\Bancaire;

use Illuminate\Database\Eloquent\Model;

class Versement extends Model
{
    protected $table = 'versements';
    public $timestamps = true;
    protected $fillable = [
        'numero_versement',
        'client_banque_id',
        'montant',
        'date_versement',
    ];

    public function clientBanque(){
        return $this->belongsTo(ClientBanque::class);
    }
}
