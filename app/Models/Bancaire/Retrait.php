<?php

namespace App\Models\Bancaire;

use Illuminate\Database\Eloquent\Model;

class Retrait extends Model
{
    protected $table = 'retraits';
    public $timestamps = true;
    protected $fillable = [
        'numero_retrait',
        'client_banque_id',
        'montant',
        'numero_cheque',
        'date_retrait',
    ];

    public function clientBanque(){
        return $this->belongsTo(ClientBanque::class);
    }
}
