<?php

namespace App\Models\Bancaire;

use Illuminate\Database\Eloquent\Model;

class ClientBanque extends Model
{
    protected $table = 'client_banques';
    public $timestamps = true;
    protected $fillable = [
        'nom',
        'prenom',
        'adresse',
        'cin',
        'contact',
        'numero_compte',
        'solde',
    ];

    public function versements(){
        return $this->hasMany(Versement::class);
    }
    public function retraits(){
        return $this->hasMany(Retrait::class);
    }
}
