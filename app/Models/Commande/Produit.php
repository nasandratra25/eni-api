<?php

namespace App\Models\Commande;

use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    protected $table = 'produits';
    public $timestamps = true;
    protected $fillable = [
        'numero_produit',
        'designation',
        'prix_unitaire',
        'stock',
    ];

    public function ventes()
    {
       return $this->hasMany(Vente::class);
    }

    public function approvisionnements()
    {
       return $this->hasMany(Approvisionnement::class);
    }

    public function clients()
    {
        return $this->belongsToMany(ClientCommande::class);
    }
}
