<?php

namespace App\Models\Commande;

use Illuminate\Database\Eloquent\Model;

class Approvisionnement extends Model
{
    protected $table = 'approvisionnements';
    public $timestamps = true;
    protected $fillable = [
        'numero_approvisionnement',
        'produit_id',
        'fournisseur_id',
        'quantite',
        'date_approvisionnement',
    ];

    public function produit()
    {
        return $this->belongsTo(Produit::class);
    }
}
