<?php

namespace App\Models\Commande;

use Illuminate\Database\Eloquent\Model;

class Vente extends Model
{
    protected $table = 'ventes';
    public $timestamps = true;
    protected $fillable = [
        'numero_vente',
        'produit_id',
        'client_commande_id',
        'quantite',
        'date_vente',
    ];

    public function produits()
    {
        return $this->belongsTo(Produit::class);
    }
    public function clients()
    {
        return $this->belongsTo(ClientCommande::class);
    }
}
