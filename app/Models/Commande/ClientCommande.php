<?php

namespace App\Models\Commande;

use Illuminate\Database\Eloquent\Model;

class ClientCommande extends Model
{
    protected $table = 'client_commandes';
    public $timestamps = true;
    protected $fillable = [
        'numero_client',
        'nom',
        'prenom',
        'adresse',
        'cin',
        'contact',
    ];

    /**
     * Get all of the reservation for the itineraire.
     */
    public function ventes()
    {
        return $this->hasMany(Vente::class);
    }

    public function produits()
    {
        return $this->belongsToMany(Produit::class);
    }

}
