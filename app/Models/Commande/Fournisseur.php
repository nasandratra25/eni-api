<?php

namespace App\Models\Commande;

use Illuminate\Database\Eloquent\Model;

class Fournisseur extends Model
{
    protected $table = 'fournisseurs';
    public $timestamps = true;
    protected $fillable = [
        'numero_fournisseur',
        'nom',
        'prenom',
        'adresse',
        'cin',
        'contact',
    ];
}
