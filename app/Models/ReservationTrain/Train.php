<?php

namespace App\Models\ReservationTrain;

use Illuminate\Database\Eloquent\Model;

class Train extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'trains';
    public $timestamps = true;
    protected $fillable = [
      'numero_train',
      'designation',
      'nombre_palces',
      'itineraire_id',
    ];

    /**
     * Get all of the reservation for the itineraire.
     */
    public function itineraires()
    {
        return $this->belongsTo(Itineraire::class);
    }

    public function places()
    {
        return $this->hasMany(Place::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
}
