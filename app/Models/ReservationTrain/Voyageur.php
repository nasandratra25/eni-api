<?php

namespace App\Models\ReservationTrain;

use Illuminate\Database\Eloquent\Model;

class Voyageur extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'voyageurs';
    public $timestamps = true;
    protected $fillable = [
        'nom',
        'prenom',
        'adresse',
        'cin',
        'contact'
    ];
}
