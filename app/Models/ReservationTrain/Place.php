<?php

namespace App\Models\ReservationTrain;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'places';
    public $timestamps = true;
    protected $fillable = [
        'numero_place',
        'occupation',
        'train_id',
    ];

    public function train()
    {
        return $this->BelongTo(Train::class);
    }

}
