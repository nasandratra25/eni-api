<?php

namespace App\Models\ReservationTrain;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reservations';
    public $timestamps = true;
    protected $fillable = [
        'numero_reservation',
        'train_id',
        'place_id',
        'voyageur_id',
        'date_reservation'
    ];

    /**
     * Get all of the reservation for the itineraire.
     */
    public function trains()
    {
        return $this->belongsTo( Train::class);
    }
}
