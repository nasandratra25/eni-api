<?php

namespace App\Models\ReservationTrain;

use Illuminate\Database\Eloquent\Model;

class Itineraire extends Model
{
    protected $table = "itineraires";
    public $timestamps = true;
    protected $fillable = [
        'numero_itineraire',
        'ville_depart',
        'ville_arrive',
        'frais'
    ];

    /**
     * Get all of the reservation for the itineraire.
     */
    public function reservations()
    {
        return $this->hasManyThrough(Reservation::class, Train::class);
    }


}
