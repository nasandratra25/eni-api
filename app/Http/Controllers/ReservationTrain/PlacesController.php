<?php

namespace App\Http\Controllers\ReservationTrain;

use App\Http\Controllers\Controller;
use App\Models\ReservationTrain\Place;
use App\Models\ReservationTrain\Reservation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class PlacesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $places = Place::get();
        return response()->json($places,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(),[
            'numero_place'=>['required'],
            'occupation'=>['required'],
            'train_id'=>['required', 'numeric'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }
        $num = $request->get('numero_place');
        $train_id = $request->get('train_id');
        $place_exist = Place::where('numero_place', '=', $num)->where('train_id', '=', $train_id)->first();
        if (is_null($place_exist)){
            $place = new Place();
            $place->numero_place = $request->get('numero_place');
            $place->occupation = $request->get('occupation');
            $place->train_id = $request->get('train_id');

            $place->save();
            $places = Place::get();
            return response()->json($places,200);
        }
        return response()->json(['Erreur'=>'Cette place existe déjà'],400);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $place = Place::find($id);
        return response()->json($place,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $place = Place::find($id);
        return response()->json($place,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'numero_place'=>['required'],
            'occupation'=>['required'],
            'train_id'=>['required', 'numeric'],
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }
        $place = Place::find($id);
        if(!$place){
            return response()->json(['Erreur'=>'Place non disponible'], 204);
        }
        $place->numero_place = $request->get('numero_place');
        $place->occupation = $request->get('occupation');
        $place->train_id = $request->get('train_id');

        $place->save();
        $places = Place::get();
        return response()->json($places,201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $place = Place::find($id);
        if(!$place){
            return response()->json(['Erreur'=>'Place non disponible'], 204);
        }
        $reservation_enleve = Reservation::where('train_id', '=', $place->train_id)->where('place_id', '=', $place->id)->delete();
        $place->delete();
        $places = Place::get();
        return response()->json($places, 200);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function placeDisponible($id)
    {
        $place = Place::where('occupation', '=', 'Disponible');
        return response()->json($place,200);

    }

    /**
     * @param int $id
     * @return Response
     */
    public function placeOccupe()
    {
        $place = Place::where('occupation', '=', 'Occupé');
        return response()->json($place,200);

    }
}
