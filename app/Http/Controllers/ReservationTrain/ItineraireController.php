<?php

namespace App\Http\Controllers\ReservationTrain;

use App\Http\Controllers\Controller;
use App\Models\ReservationTrain\Itineraire;
use App\Models\ReservationTrain\Place;
use App\Models\ReservationTrain\Reservation;
use App\Models\ReservationTrain\Train;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ItineraireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itineraires = Itineraire::get();
        return response()->json($itineraires, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(), [
            'numero_itineraire'=>['bail','required', 'unique:itineraires', 'min:10'],
            'ville_depart'=>'required',
            'ville_arrive'=>'required',
            'frais'=>'required|numeric'
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(), 400);
        }

        $itineraire = new Itineraire([
            'numero_itineraire' => $request->get('numero_itineraire'),
            'ville_depart' => $request->get('ville_depart'),
            'ville_arrive' => $request->get('ville_arrive'),
            'frais' => $request->get('frais')
        ]);
        $itineraire->save();
        $itineraires = Itineraire::get();
        return response()->json($itineraires, 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $itinéraire = Itineraire::find($id);
        return response()->json($itinéraire, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itinéraire = Itineraire::find($id);
        return response()->json($itinéraire, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(), [
            'numero_itineraire'=>['bail','required', 'min:10'],
            'ville_depart'=>'required',
            'ville_arrive'=>'required',
            'frais'=>'required|numeric'
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(), 204);
        }
        $itineraire = Itineraire::find($id);
        if(!$itineraire){
            return response()->json(['Erreur'=>'Itineraire non disponible'], 204);
        }
        $itineraire->numero_itineraire =  $request->get('numero_itineraire');
        $itineraire->ville_depart = $request->get('ville_depart');
        $itineraire->ville_arrive = $request->get('ville_arrive');
        $itineraire->frais = $request->get('frais');
        if ($itineraire->isDirty(['numero_itineraire'])){
            $numero_itineraire_exist = Itineraire::where('numero_itineraire', '=', $request->get('numero_itineraire'))->first();
            if (!is_null($numero_itineraire_exist)){
                return response()->json(['Erreur'=>'Ce numero existe deja'], 400);
            }
            $itineraire->save();
            $itineraires = Itineraire::get();
            return response()->json($itineraires,200);
        }

        $itineraire->save();
        $itineraires = Itineraire::get();
        return response()->json($itineraires, 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itineraire = Itineraire::find($id);
        if(!$itineraire){
            return response()->json(['Erreur'=>'Itineraire non disponible'], 204);
        }
        $train_itine = Train::where('itineraire_id', '=', $id)->get();
        foreach ($train_itine as $train){
            $place_train_itine = Place::where('train_id', '=', $train->id)->get();
            foreach ($place_train_itine as $place){
                Reservation::where('train_id', '=', $place->train_id)->where('place_id', '=', $place->id)->delete();
                $place->delete();
            }
            $train->delete();
        }
        $itineraire->delete();
        $itineraires = Itineraire::get();
        return response()->json($itineraires, 200);
    }

    /**
     *
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function itineraireReserver($id)
    {
        $itineraire = Itineraire::find($id);
        $itineraireReserver = $itineraire->reservations;
        return response()->json($itineraireReserver, 200);
    }
}
