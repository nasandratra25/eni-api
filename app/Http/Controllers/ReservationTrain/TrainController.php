<?php

namespace App\Http\Controllers\ReservationTrain;

use App\Http\Controllers\Controller;
use App\Models\ReservationTrain\Itineraire;
use App\Models\ReservationTrain\Place;
use App\Models\ReservationTrain\Reservation;
use App\Models\ReservationTrain\Train;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class TrainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $trains = Train::get();
        return response()->json($trains,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $createValidation = Validator::make($request->all(),[
            'numero_train'=>['required','unique:trains','min:15'],
            'designation'=>['required'],
            'nombre_places'=>['required','numeric'],
            'itineraire_id'=>['required', 'numeric'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }

        $train = new Train();
        $train->numero_train = $request->get('numero_train');
        $designation_exist = Train::where('designation', '=', $request->get('designation'))->first();
        if (!is_null($designation_exist)){
            return response()->json(['message'=>'Ce nom de train existe deja'], 400);
        }
        $train->designation = $request->get('designation');
        $train->nombre_places = $request->get('nombre_places');
        $train->itineraire_id = $request->get('itineraire_id');
        $train->save();
        $dernier_train = Train::where('numero_train', '=', $request->get('numero_train'))->first();
        for($i = 1; $i <= $dernier_train->nombre_places; $i++) {
            $place = new Place();
            $place->numero_place = $i;
            $place->occupation = 'Disponible';
            $place->train_id = $dernier_train->id;
            $place->save();
        }

        $trains = Train::get();
        return response()->json([$trains, 'dernier entree' => $dernier_train],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $train = Train::find($id);
        return response()->json($train,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $train = Train::find($id);
        return response()->json($train,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'numero_train'=>['required','min:15'],
            'designation'=>['required'],
            'nombre_places'=>['required','numeric'],
            'itineraire_id'=>['required', 'numeric'],
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }
        $train = Train::find($id);
        if(!$train){
            return response()->json(['Erreur'=>'Train non disponible'], 204);
        }
        $train->numero_train = $request->get('numero_train');
        $train->designation = $request->get('designation');
        $train->nombre_places = $request->get('nombre_places');
        $train->itineraire_id = $request->get('itineraire_id');

        if ($train->isDirty(['designation'])){
            $designation_exist = Train::where('designation', '=', $request->get('designation'))->first();
            if (!is_null($designation_exist)){
                return response()->json(['Erreur'=>'Ce nom de train existe deja'], 400);
            }
            $train->save();
            $trains = Train::get();
            return response()->json($trains,200);
        }

        if ($train->isDirty(['nombre_places'])){
            $update_train_old = Train::where('id', '=', $id)->first();
            $nb_place_old = $update_train_old->nombre_places;
            if ($request->get('nombre_places') < $nb_place_old ){
                $limitAffiche =  $nb_place_old - $request->get('nombre_places');
                $place_enleve = Place::where('train_id', '=', $id)->orderBy('id', 'DESC')->limit($limitAffiche)->get();
                foreach ($place_enleve as $place ){
                    $reservation_enleve = Reservation::where('train_id', '=', $place->train_id)->where('place_id', '=', $place->id)->delete();
                    $place->delete();
                }
            }
            $nb_place_ajoute = $request->get('nombre_places') - $nb_place_old;
            for($i = 1; $i <= $nb_place_ajoute; $i++) {
                $place_ajout = Place::where('train_id', '=', $id)->orderBy('id', 'DESC')->limit(1)->first();
                $place = new Place();
                $place->numero_place = $place_ajout->numero_place + 1;
                $place->occupation = 'Disponible';
                $place->train_id = $place_ajout->train_id;
                $place->save();

            }
        }
        $train->save();
        $trains = Train::get();
        return response()->json($trains,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $train = Train::find($id);
        if(!$train){
            return response()->json(['Erreur'=>'Train non disponible'], 204);
        }
        Reservation::where('train_id', '=', $id)->delete();
        Place::where('train_id', '=', $id)->delete();
        $train->delete();
        $trains = Train::get();
        return response()->json($trains, 200);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function trainPlace($id)
    {
        $train = Train::find($id);
        if (!$train){
            return response()->json(['Erreur'=>'Train non disponible'], 204);
        }
        $placeTrain = $train->places;
        return response()->json($placeTrain,200);

    }
    /**
     * @param int $id
     * @return Response
     */
    public function trainPlaceDisponible($id)
    {
        $train = Train::find($id);
        if (!$train){
            return response()->json(['Erreur'=>'Train non disponible'], 204);
        }
        $placeTrain = Place::where('train_id', '=', $id)->where('occupation', '=', 'Disponible')->get();
        return response()->json($placeTrain,200);

    }

    /**
     * @param int $id
     * @param int $idplace
     * @return Response
     */
    public function trainPlaceReserver($id, $idplace)
    {
        $placeEdit = [];
        $place = Place::find($idplace);
        if (!$place){
            return response()->json(['Erreur'=>'Place non disponible'], 204);
        }
        $placeTrain = Place::where('train_id', '=', $id)->where('id', '=', $idplace)->get();
        $placeTrainD = Place::where('train_id', '=', $id)->where('occupation', '=', 'Disponible')->get();
        foreach ($placeTrain as $place){
            array_push($placeEdit, $place);
        }
        foreach ($placeTrainD as $place){
        array_push($placeEdit, $place);
        }
        return response()->json($placeEdit,200);

    }
    /**
     * @param int $id
     * @return Response
     */
    public function trainPlaceOccupe($id)
    {
        $train = Train::find($id);
        if (!$train){
            return response()->json(['Erreur'=>'Train non disponible'], 204);
        }
        $placeTrain = Place::where('train_id', '=', $id)->where('occupation', '=', 'Occupé')->get();
        return response()->json($placeTrain,200);

    }

    /**
     * @param int $id
     * @return Response
     */
    public function trainRecetteAnnuel($id, Request $request)
    {
        $train = Train::find($id);
        if (!$train){
            return response()->json(['Erreur'=>'Train non disponible'], 204);
        }

        $itinéraireFrais = Itineraire::find($train->itineraire_id)->frais;
        $nbReservation = Reservation::where('train_id','=', $id)
            ->whereYear('date_reservation', '=', (date('Y',strtotime($request->get('date_recette')))))
            ->count();
        $recetteAnnuel = floatval($itinéraireFrais) * floatval($nbReservation);
        return response()->json(['recette'=> $recetteAnnuel],200);

    }
    /**
     * @param int $id
     * @return Response
     */
    public function trainRecetteMensuel($id, Request $request)
    {
        $train = Train::find($id);
        if (!$train){
            return response()->json(['Erreur'=>'Train non disponible'], 204);
        }
        $recette = [];
        $liste_mois = ['Janvier', 'Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre', 'Decembre' ];
        $date_recette = date('Y',strtotime($request->get('date_recette')));
        //$mois_recette = date('m',strtotime($request->get('date_recette'))) ;
        for ($i = 1; $i <= 12; $i++){
            $itinéraireFrais = Itineraire::find($train->itineraire_id)->frais;
            $nbReservation = Reservation::where('train_id','=', $id)
                ->whereYear('date_reservation', '=', $date_recette)
                ->whereMonth('date_reservation', '=', $i)
                ->count();
            $recetteMensuel = floatval($itinéraireFrais) * floatval($nbReservation);
            array_push($recette,['recette'=> $recetteMensuel, 'mois' => $liste_mois[$i-1]]);
        }

        return response()->json($recette,200);

    }
}
