<?php

namespace App\Http\Controllers\ReservationTrain;

use App\Http\Controllers\Controller;
use App\Models\ReservationTrain\Place;
use App\Models\ReservationTrain\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = Reservation::get();
        return response()->json($reservations,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(),[
            'numero_reservation'=>['required','unique:reservations','min:9'],
            'train_id'=>['required','numeric'],
            'place_id'=>['required','numeric'],
            'voyageur_id'=>['required', 'numeric'],
            'date_reservation'=>['required', 'date'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }

        /*$reservation = new Reservation([
            'numero_reservation' => $request->get('numero_reservation'),
            'train_id' => $request->get('train'),
            'place_id' => $request->get('place'),
            'voyageur_id' => $request->get('date_reservation'),
            'date_reservation' => $request->get('date_reservation'),
        ]);*/
        $reservation = new Reservation();
        $reservation->numero_reservation = $request->get('numero_reservation');
        $reservation->train_id = $request->get('train_id');
        $reservation->place_id = $request->get('place_id');
        $reservation->voyageur_id = $request->get('voyageur_id');
        $reservation->date_reservation = $request->get('date_reservation');
        $reservation->save();

        $placeReserver = Place::where('train_id','=',$request->get('train_id'))
            ->where('id', '=', $request->get('place_id'))
            ->update([
                'occupation' => 'Occupé'
            ]);
        $reservation = Reservation::get();
        return response()->json($reservation,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reservation = Reservation::find($id);
        return response()->json($reservation,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reservation = Reservation::find($id);
        return response()->json($reservation,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'numero_reservation'=>['required','min:9'],
            'train_id'=>['required','numeric'],
            'place_id'=>['required','numeric'],
            'voyageur_id'=>['required', 'numeric'],
            'date_reservation'=>['required', 'date'],
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }
        $reservation = Reservation::find($id);
        if(!$reservation){
            return response()->json(['Erreur'=>'Reservation non disponible'], 204);
        }
        $reservation->numero_reservation = $request->get('numero_reservation');
        $reservation->train_id = $request->get('train_id');
        $reservation->place_id = $request->get('place_id');
        $reservation->voyageur_id = $request->get('voyageur_id');
        $reservation->date_reservation = $request->get('date_reservation');

        if ($reservation->isDirty(['numero_reservation'])){
            $numero_reservation_exist = Itineraire::where('numero_reservation', '=', $request->get('numero_reservation'))->first();
            if (!is_null($numero_reservation_exist)){
                return response()->json(['Erreur'=>'Ce numero dereservation existe deja'], 400);
            }
            $reservation->save();
            $reservations = Reservation::get();
            return response()->json($reservations,200);
        }
        if($reservation->isDirty(['train_id', 'place_id','numero_reservation'])){
            $reservationOld = Reservation::find($id);
            $placeReserverOld = Place::where('train_id','=',$reservationOld->train_id)
                ->where('id', '=', $reservationOld->train_id)
                ->update([
                    'occupation' => 'Disponible'
                ]);
        }
        if($reservation->isDirty(['place_id'])){
            $reservationOld = Reservation::find($id);
            $placeReserverOld = Place::where('train_id','=',$reservationOld->train_id)
                ->where('id', '=', $reservationOld->train_id)
                ->update([
                    'occupation' => 'Disponible'
                ]);
        }
        $reservation->save();
        $placeReserver = Place::where('train_id','=',$request->get('train_id'))
            ->where('id', '=', $request->get('place_id'))
            ->update([
                'occupation' => 'Occupé'
            ]);
        $reservations = Reservation::get();
        return response()->json($reservations,201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reservation = Reservation::find($id);
        if(!$reservation){
            return response()->json(['Erreur'=>'Reservation non disponible'], 204);
        }
        $placeReserver = Place::where('train_id','=',$reservation->train_id)
            ->where('id', '=', $reservation->place_id)
            ->update([
                'occupation' => 'Disponible'
            ]);
        $reservation->delete();
        $reservations = Reservation::get();
        return response()->json($reservations, 200);
    }
}
