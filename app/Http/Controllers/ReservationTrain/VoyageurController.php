<?php

namespace App\Http\Controllers\ReservationTrain;

use App\Http\Controllers\Controller;
use App\Models\ReservationTrain\Reservation;
use App\Models\ReservationTrain\Voyageur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VoyageurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $voyageurs = Voyageur::get();
        return response()->json($voyageurs, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $createValidation = Validator::make($request->all(), [
            'nom'=>['required'],
            'prenom'=>'required',
            'adresse'=>'required',
            'cin'=>['required','min:13','max:13'],
            'contact'=>['required','min:10'],
        ]);

        if ($createValidation->fails())
        {
            return response()->json(['Erreur'=> $createValidation->errors()], 400);
        }
        $nom = $request->get('nom');
        $prenom = $request->get('prenom');
        $voyageur_exist = Voyageur::where('nom', '=', $nom)->where('prenom', '=', $prenom)->first();
        if (is_null($voyageur_exist)){
            $voyageur = new Voyageur([
                'nom' => $request->get('nom'),
                'prenom' => $request->get('prenom'),
                'adresse' => $request->get('adresse'),
                'cin' => $request->get('cin'),
                'contact' => $request->get('contact'),
            ]);
            $voyageur->save();
            $voyageurs = Voyageur::get();
            return response()->json($voyageurs, 201);
        }
        return response()->json(['Erreur'=>'Cette voyageur existe déjà'],400);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Voyageur = Voyageur::find($id);
        return response()->json($Voyageur, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Voyageur = Voyageur::find($id);
        return response()->json($Voyageur, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(), [
            'nom'=>['required'],
            'prenom'=>'required',
            'adresse'=>'required',
            'cin'=>['required','min:13','max:13'],
            'contact'=>['required','min:10'],
        ]);

        if ($updateValidation->fails())
        {
            return response()->json(['Erreur'=> $updateValidation->errors()], 400);
        }

            $voyageur = Voyageur::find($id);
        if(!$voyageur){
            return response()->json(['Erreur'=>'Voyageur non disponible'], 204);
        }
            $voyageur->nom =  $request->get('nom');
            $voyageur->prenom = $request->get('prenom');
            $voyageur->adresse = $request->get('adresse');
            $voyageur->cin = $request->get('cin');
            $voyageur->contact= $request->get('contact');
            $voyageur->save();
            $voyageurs = Voyageur::get();
            return response()->json($voyageurs, 200);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $voyageur = Voyageur::find($id);
        if(!$voyageur){
            return response()->json(['Erreur'=>'Voyageur non disponible'], 204);
        }
        Reservation::where('voyageur_id', '=', $id)->delete();
        $voyageur->delete();
        $voyageurs = Voyageur::get();
        return response()->json($voyageurs, 200);
    }
}
