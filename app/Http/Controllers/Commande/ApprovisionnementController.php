<?php

namespace App\Http\Controllers\Commande;

use App\Http\Controllers\Controller;
use App\Models\Commande\Approvisionnement;
use App\Models\Commande\Fournisseur;
use App\Models\Commande\Produit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApprovisionnementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return IlluminateHttpResponse
     */
    public function index()
    {
        $approvisionnements = Approvisionnement::get();
        return response()->json($approvisionnements, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return IlluminateHttpResponse
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(),[
            'numero_approvisionnement'=>['required','unique:approvisionnements'],
            'produit_id'=>['required'],
            'fournisseur_id'=>['required'],
            'quantite'=>['required','numeric', 'min:1'],
            'date_approvisionnement'=>['required','date'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }
        $approvisionnement = new Approvisionnement();
        $approvisionnement->numero_approvisionnement = $request->get('numero_approvisionnement');
        $approvisionnement->produit_id = $request->get('produit_id');
        $approvisionnement->fournisseur_id = $request->get('fournisseur_id');
        $approvisionnement->quantite = $request->get('quantite');
        $approvisionnement->date_approvisionnement = $request->get('date_approvisionnement');

        $ancienStock = Produit::find($request->get('produit_id'))->stock;
        $noveauStock = floatval($ancienStock) + floatval($request->get('quantite'));
        $stockProduit = Produit::where('id','=',$request->get('produit_id'))
            ->update([
                'stock' => $noveauStock
            ]);
        $approvisionnement->save();
        $approvisionnements = Approvisionnement::get();
        return response()->json($approvisionnements,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {
        $approvisionnement = Approvisionnement::find($id);
        if (!$approvisionnement)
        {
            return response()->json(['Erreur'=>'Approvisionnement non disponible'], 204);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'numero_approvisionnement'=>['required'],
            'produit_id'=>['required'],
            'fournisseur_id'=>['required'],
            'quantite'=>['required','numeric', 'min:1'],
            'date_approvisionnement'=>['required','date'],
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }
        $approvisionnement = Approvisionnement::find($id);
        if (!$approvisionnement)
        {
            return response()->json(['Erreur'=>'Versement non disponible'], 204);
        }
        $ancienstockApprovisionnement = $approvisionnement->quantite;
        $approvisionnement->numero_approvisionnement = $request->get('numero_approvisionnement');
        $approvisionnement->produit_id = $request->get('produit_id');
        $approvisionnement->fournisseur_id = $request->get('fournisseur_id');
        $approvisionnement->quantite = $request->get('quantite');
        $approvisionnement->date_approvisionnement = $request->get('date_approvisionnement');

        if ($approvisionnement->isDirty(['numero_approvisionnement'])){
            $numero_approvisionnement_exist = Approvisionnement::where('numero_approvisionnement', '=', $request->get('numero_approvisionnement'))->first();
            if (!is_null($numero_approvisionnement_exist)){
                return response()->json(['Erreur'=>'Ce numero approvisionnement existe deja'], 400);
            }
        }

        if ($approvisionnement->isDirty(['produit_id'])){
            $approvisionnementOld = Approvisionnement::find($id);
            $ancienProduit = $approvisionnementOld->produit_id;
            $ancienStockOld = Produit::find($ancienProduit)->stock;
            $nouveauStockOld = floatval($ancienStockOld) - floatval($ancienstockApprovisionnement);
            $stockProduit = Produit::where('id','=',$ancienProduit)
                ->update([
                    'stock' => $nouveauStockOld
                ]);
            $ancienStock = Produit::find($request->get('produit_id'))->stock;
            $nouveauStock = floatval($ancienStock) + floatval($request->get('quantite'));
            $stockProduit = Produit::where('id','=',$request->get('produit_id'))
                ->update([
                    'stock' => $nouveauStock
                ]);
            $approvisionnement->save();
            $approvisionnements = Approvisionnement::get();
            return response()->json($approvisionnements,200);
        }

        $ancienStock = Produit::find($request->get('produit_id'))->stock;
        $nouveauStock = floatval($ancienStock) + floatval($request->get('quantite')) - floatval($ancienstockApprovisionnement);
        $stockProduit = Produit::where('id','=',$request->get('produit_id'))
            ->update([
                'stock' => $nouveauStock
            ]);
        $approvisionnement->save();
        $approvisionnements = Approvisionnement::get();
        return response()->json($approvisionnements,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        $approvisionnement = Approvisionnement::find($id);
        if (!$approvisionnement)
        {
            return response()->json(['Erreur'=>'approvisionnement non disponible'], 204);
        }
        $ancienstockAppro = $approvisionnement->quantite;
        $ancienProduitID = $approvisionnement->produit_id;
        $ancienProduit = Produit::find($ancienProduitID);
        $sommeVente = (float)$ancienProduit->ventes->sum('quantite');
        $sommeAppro = (float)$ancienProduit->approvisionnements->sum('quantite') - (float)$ancienstockAppro;
        if (!($sommeVente > $sommeAppro)) {
            $ancienStock = $ancienProduit->stock;
            $nouveauStock = floatval($ancienStock) - floatval($ancienstockAppro);
            $stockProduit = Produit::where('id','=',$ancienProduitID)
                ->update([
                    'stock' => $nouveauStock
                ]);
            $approvisionnement->delete();
            $approvisionnements = Approvisionnement::get();
            return response()->json($approvisionnements,200);
        }
        return response()->json(['Attention' => 'Le stock est Insuffisant apres cette suppression!'], 400);

    }
}
