<?php

namespace App\Http\Controllers\Commande;

use App\Http\Controllers\Controller;
use App\Models\Commande\Approvisionnement;
use App\Models\Commande\Produit;
use App\Models\Commande\Vente;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produits = Produit::get();
        return response()->json($produits,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(),[
            'numero_produit'=>['required', 'unique:produits'],
            'designation'=>['required'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }
        $produit = new Produit();
        $produit->numero_produit = $request->get('numero_produit');
        $produit->designation = $request->get('designation');
        if (!is_null($request->get('prix_unitaire'))){
            $produit->prix_unitaire = $request->get('prix_unitaire');
        }
        if (!is_null($request->get('stock'))){
            $produit->stock = $request->get('stock');
        }
        $produit->save();

        $produits = Produit::get();
        return response()->json($produits,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produit = Produit::find($id);
        return response()->json($produit,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'numero_produit'=>['required'],
            'designation'=>['required'],
            'prix_unitaire'=>['required', 'numeric'],
            ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }
        $produit = Produit::find($id);
        if (!$produit){
            return response()->json(['Erreur'=>'Le produit non disponible'], 204);
        }
        $produit->numero_produit = $request->get('numero_produit');
        $produit->designation = $request->get('designation');
        if (!is_null($request->get('prix_unitaire'))){
            $produit->prix_unitaire = $request->get('prix_unitaire');
        }
        if (!is_null($request->get('stock'))){
            $produit->stock = $request->get('stock');
        }

        if ($produit->isDirty(['numero_produit'])){
            $num_prod_exist = Produit::where('numero_produit', '=', $request->get('numero_produit'))->first();
            if (!is_null($num_prod_exist)){
                return response()->json(['Erreur'=>'Ce numero existe deja'], 400);
            }
            $produit->save();
            $produits = Produit::get();
            return response()->json($produits,200);
        }
        $produit->save();
        $produits = Produit::get();
        return response()->json($produits,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produit = Produit::find($id);
        if (!$produit){
            return response()->json(['Erreur'=>'Le produit non disponible'], 204);
        }
        $produitAppro = Approvisionnement::where('produit_id', '=', $id)->delete();
        $produitVente = Vente::where('produit_id', '=', $id)->delete();
        $produit->delete();
        $produits = Produit::get();
        return response()->json($produits,200);
    }

    /**
     * Search the specified resource from storage.
     *
     *
     * @return Response
     */
    public function rechercheProduits(Request $request)
    {
        $motRechercher = $request->get('mot_recherche');
        $produits = Produit::where(function($query) use ($motRechercher){
            $query->where('numero_produit', 'LIKE', '%'.$motRechercher.'%');
            $query->orWhere('designation', 'LIKE', '%'.$motRechercher.'%');
        })->get();
        return response()->json($produits,200);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function mouvementVentAnnuel($id, Request $request)
    {
        $produit = Produit::find($id);
        if (!$produit){
            return response()->json(['Erreur'=>'Produit non disponible'], 400);
        }
        $produitID = $produit->id;
        $vente = Vente::where('produit_id','=',$produitID)
            ->whereYear('date_vente', '=', (date('Y',strtotime($request->get('date_mouvement')))))
            ->get();

        return response()->json($vente,200);

    }
    /**
     * @param int $id
     * @return Response
     */
    public function mouvementApprovisionnementAnnuel($id, Request $request)
    {
        $produit = Produit::find($id);
        if (!$produit){
            return response()->json(['Erreur'=>'Produit non disponible'], 400);
        }
        $produitID = $produit->id;
        $approvisionnement = Approvisionnement::where('produit_id','=',$produitID)
            ->whereYear('date_approvisionnement', '=', (date('Y',strtotime($request->get('date_mouvement')))))
            ->get();

        return response()->json($approvisionnement,200);

    }

    /**
     * @param int $id
     * @return Response
     */
    public function mouvementVentMensuel($id, Request $request)
    {
        $produit = Produit::find($id);
        if (!$produit){
            return response()->json(['Erreur'=>'Produit non disponible'], 400);
        }
        $produitID = $produit->id;

        $vente = Vente::where('produit_id','=',$produitID)
            ->whereYear('date_vente', '=', (date('Y',strtotime($request->get('date_mouvement')))))
            ->get();

        return response()->json($vente,200);

    }
    /**
     * @param int $id
     * @return Response
     */
    public function mouvementApprovisionnementMensuel($id, Request $request)
    {
        $produit = Produit::find($id);
        if (!$produit){
            return response()->json(['Erreur'=>'Produit non disponible'], 400);
        }
        $produitID = $produit->id;
        $approvisionnement = Approvisionnement::where('produit_id','=',$produitID)
            ->whereYear('date_approvisionnement', '=', (date('Y',strtotime($request->get('date_mouvement')))))
            ->whereMonth('date_approvisionnement', '=', (date('m',strtotime($request->get('date_mouvement')))))
            ->get();

        return response()->json($approvisionnement,200);

    }

    /**
     * @param int $id
     * @return Response
     */
    public function mouvementVent2Date($id, Request $request)
    {
        $dateValidation = Validator::make($request->all(),[
            'date_debut'=>['required','date'],
            'date_fin'=>['required','date'],
        ]);
        if ($dateValidation->fails()){
            return response()->json($dateValidation->errors(),400);
        }
        $date_debut = $request->get('date_debut');
        $date_fin = $request->get('date_fin');

        $produit = Produit::find($id);
        if (!$produit){
            return response()->json(['Erreur'=>'Produit non disponible'], 400);
        }
        $produitID = $produit->id;

        $vente = Vente::where('produit_id','=',$produitID)
            ->whereBetween('date_vente',  [$date_debut,$date_fin])
            ->get();


        return response()->json($vente,200);

    }
/**
     * @param int $id
     * @return Response
     */
    public function mouvementApprovisionnement2Date($id, Request $request)
    {
        $dateValidation = Validator::make($request->all(),[
            'date_debut'=>['required','date'],
            'date_fin'=>['required','date'],
        ]);
        if ($dateValidation->fails()){
            return response()->json($dateValidation->errors(),400);
        }
        $date_debut = $request->get('date_debut');
        $date_fin = $request->get('date_fin');

        $produit = Produit::find($id);
        if (!$produit){
            return response()->json(['Erreur'=>'Produit non disponible'], 400);
        }
        $produitID = $produit->id;
        $approvisionnement = Approvisionnement::where('produit_id','=',$produitID)
            ->whereBetween('date_approvisionnement',  [$date_debut,$date_fin])
            ->get();


        return response()->json($approvisionnement,200);

    }
    /**
         * @param int $id
         * @return Response
         */
        public function mouvementTotal($id, Request $request)
        {
            $dateValidation = Validator::make($request->all(),[
                'date_debut'=>['required','date'],
                'date_fin'=>['required','date'],
            ]);
            if ($dateValidation->fails()){
                return response()->json($dateValidation->errors(),400);
            }
            $date_debut = $request->get('date_debut');
            $date_fin = $request->get('date_fin');

            $produit = Produit::find($id);
            if (!$produit){
                return response()->json(['Erreur'=>'Produit non disponible'], 400);
            }
            $produitID = $produit->id;

            $sommeAppro = (float)Approvisionnement::where('produit_id','=',$produitID)
                ->whereBetween('date_approvisionnement', [$date_debut,$date_fin])
                ->sum('quantite');
            $sommeVente = (float)Vente::where('produit_id','=',$produitID)
                ->whereBetween('date_vente', [$date_debut,$date_fin])
                ->sum('quantite');
            $total = ['totalEntree' => $sommeAppro, 'totalSortie'=>$sommeVente];

            return response()->json($total,200);

        }
    /**
     * @param int $id
     * @return Response
     */
    public function mouvementTotalAnnuel($id, Request $request)
    {
        $produit = Produit::find($id);
        if (!$produit){
            return response()->json(['Erreur'=>'Produit non disponible'], 400);
        }
        $produitID = $produit->id;

        $sommeAppro = (float)Approvisionnement::where('produit_id','=',$produitID)
            ->whereYear('date_approvisionnement', '=', (date('Y',strtotime($request->get('date_mouvement')))))
            ->sum('quantite');
        $sommeVente = (float)Vente::where('produit_id','=',$produitID)
            ->whereYear('date_vente', '=', (date('Y',strtotime($request->get('date_mouvement')))))
            ->sum('quantite');
        $total = ['totalEntree' => $sommeAppro, 'totalSortie'=>$sommeVente];

        return response()->json($total,200);

    }

    /**
     * @param int $id
     * @return Response
     */
    public function mouvementTotalMensuel($id, Request $request)
    {
        $produit = Produit::find($id);
        if (!$produit){
            return response()->json(['Erreur'=>'Produit non disponible'], 400);
        }
        $produitID = $produit->id;

        $sommeAppro = (float)Approvisionnement::where('produit_id','=',$produitID)
            ->whereYear('date_approvisionnement', '=', (date('Y',strtotime($request->get('date_mouvement')))))
            ->whereMonth('date_approvisionnement', '=', (date('m',strtotime($request->get('date_mouvement')))))
            ->sum('quantite');
        $sommeVente = (float)Vente::where('produit_id','=',$produitID)
            ->whereYear('date_vente', '=', (date('Y',strtotime($request->get('date_mouvement')))))
            ->whereMonth('date_vente', '=', (date('m',strtotime($request->get('date_mouvement')))))
            ->sum('quantite');
        $total = ['totalEntree' => $sommeAppro, 'totalSortie'=>$sommeVente];

        return response()->json($total,200);

    }


}
