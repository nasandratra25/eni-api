<?php

namespace App\Http\Controllers\Commande;

use App\Http\Controllers\Controller;
use App\Models\Commande\Produit;
use App\Models\Commande\Vente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return IlluminateHttpResponse
     */
    public function index()
    {
        $ventes = Vente::get();
        return response()->json($ventes, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return IlluminateHttpResponse
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(),[
            'numero_vente'=>['required','unique:ventes'],
            'produit_id'=>['required'],
            'client_commande_id'=>['required','numeric', 'min:1'],
            'quantite'=>['required','numeric'],
            'date_vente'=>['required','date'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }
        $vente = new Vente();
        $vente->numero_vente = $request->get('numero_vente');
        $vente->produit_id = $request->get('produit_id');
        $vente->client_commande_id = $request->get('client_commande_id');
        $vente->quantite = $request->get('quantite');
        $vente->date_vente = $request->get('date_vente');

        $ancienStock = Produit::find($request->get('produit_id'))->stock;
        if (!(floatval($ancienStock) < floatval($request->get('quantite')))){
            $nouveauStock = floatval($ancienStock) - floatval($request->get('quantite'));
            $stockProduit = Produit::where('id','=',$request->get('produit_id'))
                ->update([
                    'stock' => $nouveauStock
                ]);
            $vente->save();
            $ventes = Vente::get();
            return response()->json($ventes,201);
        }
        return response()->json(['Attention' => 'Le stock es Insuffisant'], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {
        $vente = Vente::find($id);
        if (!$vente) {
            return response()->json(['Erreur' => 'Vente non disponible'], 204);
        }
        return response()->json($vente,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'numero_vente'=>['required'],
            'produit_id'=>['required'],
            'client_commande_id'=>['required','numeric', 'min:1'],
            'quantite'=>['required','numeric'],
            'date_vente'=>['required','date'],
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }
        $vente = Vente::find($id);
        if (!$vente)
        {
            return response()->json(['Erreur'=>'Retrait non disponible'], 204);
        }
        $ancienstockVente = $vente->quantite;

        $vente->numero_vente = $request->get('numero_vente');
        $vente->produit_id = $request->get('produit_id');
        $vente->client_commande_id = $request->get('client_commande_id');
        $vente->quantite = $request->get('quantite');
        $vente->date_vente = $request->get('date_vente');

        if ($vente->isDirty(['numero_vente'])){
            $numero_vente_exist = Vente::where('numero_vente', '=', $request->get('numero_vente'))->first();
            if (!is_null($numero_vente_exist)){
                return response()->json(['Erreur'=>'Ce numero vente existe deja'], 400);
            }
        }

        if ($vente->isDirty(['produit_id'])){
            $venteOld = Vente::find($id);
            $ancienProduit = $venteOld->produit_id;
            $ancienstockOld = Produit::find($ancienProduit)->stock;
            $noveaustockOld = floatval($ancienstockOld) + floatval($ancienstockVente);
            $stockProduit = Produit::where('id','=',$ancienProduit)
                ->update([
                    'stock' => $noveaustockOld
                ]);

            $ancienstock = Produit::find($request->get('produit_id'))->stock;
            if (!(floatval($ancienstock) < floatval($request->get('quantite')))) {
                $noveaustock = floatval($ancienstock) - floatval($request->get('quantite'));
                $stockProduit = Produit::where('id','=',$request->get('produit_id'))
                    ->update([
                        'stock' => $noveaustock
                    ]);
                $vente->save();
                $ventes = Vente::get();
                return response()->json($ventes,200);
            }
            return response()->json(['Attention' => 'Le stock es Insuffisant'], 400);
        }

        $ancienstock = Produit::find($request->get('produit_id'))->stock;
        $ancienstock = floatval($ancienstock) + floatval($ancienstockVente);
        if (!(floatval($ancienstock) < floatval($request->get('quantite')))) {
            $noveaustock = floatval($ancienstock) - floatval($request->get('quantite'));
            $stockProduit = Produit::where('id','=',$request->get('produit_id'))
                ->update([
                    'stock' => $noveaustock
                ]);
            $vente->save();
            $ventes = Vente::get();
            return response()->json($ventes,200);
        }
        return response()->json(['Attention' => 'Le stock es Insuffisant'], 400);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        $vente = Vente::find($id);
        if (!$vente)
        {
            return response()->json(['Erreur'=>'Vente non disponible'], 204);
        }
        $ancienstockVersement = $vente->quantite;
        $ancienProduit = $vente->produit_id;
        $ancienstock = Produit::find($ancienProduit)->stock;
        $noveaustock = floatval($ancienstock) + floatval($ancienstockVersement);
        $stockProduit = Produit::where('id','=',$ancienProduit)
            ->update([
                'stock' => $noveaustock
            ]);
        $vente->delete();
        $ventes = Vente::get();
        return response()->json($ventes,200);
    }
}
