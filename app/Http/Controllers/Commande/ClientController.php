<?php

namespace App\Http\Controllers\Commande;

use App\Http\Controllers\Controller;
use App\Models\Commande\ClientCommande;
use App\Models\Commande\Produit;
use App\Models\Commande\Vente;
use App\Models\Employee\Employe;
use App\Models\Employee\Travail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = ClientCommande::get();
        return response()->json($clients, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(),[
            'numero_client'=>['required', 'unique:client_commandes'],
            'nom'=>['required'],
            'prenom'=>['required'],
            'adresse'=>['required'],
            'contact'=>['required', 'min:10'],
            'cin'=>['required', 'min:13', 'max:13'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }
        $nom = $request->get('nom');
        $prenom = $request->get('prenom');
        $client_exist = ClientCommande::where('nom', '=', $nom)->where('prenom', '=', $prenom)->first();
        if (is_null($client_exist)){
            $client = new ClientCommande();
            $client->numero_client = $request->get('numero_client');
            $client->nom = $request->get('nom');
            $client->prenom = $request->get('prenom');
            $client->adresse = $request->get('adresse');
            $client->contact = $request->get('contact');
            $client->cin = $request->get('cin');
            $client->save();

            $clients = ClientCommande::get();
            return response()->json($clients,201);
        }
        return response()->json(['Erreur'=>'Le client existe déjà'],400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = ClientCommande::find($id);
        if (!$client)
        {
            return response()->json(['Erreur'=>'Le client n\'est pas disponible'], 204);
        }
        return response()->json($client, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'numero_client'=>['required'],
            'nom'=>['required'],
            'prenom'=>['required'],
            'adresse'=>['required'],
            'contact'=>['required', 'min:10'],
            'cin'=>['required', 'min:13', 'max:13'],
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }

        $client = ClientCommande::find($id);
        if (!$client)
        {
            return response()->json(['Erreur'=>'Le client n\'est pas disponible'], 204);
        }
        $client->numero_client = $request->get('numero_client');
        $client->nom = $request->get('nom');
        $client->prenom = $request->get('prenom');
        $client->adresse = $request->get('adresse');
        $client->contact = $request->get('contact');
        $client->cin = $request->get('cin');
        if ($client->isDirty(['numero_client'])){
            $num_cli_exist = ClientCommande::where('numero_client', '=', $request->get('numero_client'))->first();
            if (!is_null($num_cli_exist)){
                return response()->json(['Erreur'=>'Ce numero existe deja'], 400);
            }
            $client->save();
            $clients = ClientCommande::get();
            return response()->json($clients,200);
        }
        $client->save();
        $clients = ClientCommande::get();
        return response()->json($clients,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = ClientCommande::find($id);
        if (!$client)
        {
            return response()->json(['Erreur'=>'Le client n\'est pas disponible'], 204);
        }
        $venteClient = Vente::where('client_commande_id', '=', $id)->get();
        foreach ($venteClient as $clientV){
            $produitID = $clientV->produit_id;
            $ancienstockOld = Produit::find($produitID)->stock;
            $noveaustockOld = floatval($ancienstockOld) + floatval($clientV->quantite);
            $stockProduit = Produit::where('id','=',$produitID)
                ->update([
                    'stock' => $noveaustockOld
                ]);
            $clientV->delete();
        }
        $client->delete();
        $clients = ClientCommande::get();
        return response()->json($clients,201);
    }

    /**
     * Search the specified resource from storage.
     *
     *
     * @return Response
     */
    public function rechercheClients(Request $request)
    {
        $motRechercher = $request->get('mot_recherche');
        $clients = ClientCommande::where(function($query) use ($motRechercher){
            $query->where('numero_client', 'LIKE', '%'.$motRechercher.'%');
            $query->orWhere('nom', 'LIKE', '%'.$motRechercher.'%');
            $query->orWhere('prenom', 'LIKE', '%'.$motRechercher.'%');
            $query->orWhere('adresse', 'LIKE', '%'.$motRechercher.'%');
        })->get();
        return response()->json($clients,200);
    }

    /**
     *
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function venteProduitParClient($id, Request $request)
    {
        $produit = [];
        $vente = Vente::where('client_commande_id','=',$id)->get();
        foreach ($vente as $vente){
            $produitID = $vente->produit_id;
            $produitVendu_parClient = Produit::find($produitID);
            $produitVendu_pu = $produitVendu_parClient->prix_unitaire;
            $produitVendu_parClient['quantite'] = $vente->quantite;
            $produitVendu_parClient['montant'] = ($vente->quantite * $produitVendu_pu);
            array_push($produit,$produitVendu_parClient);

        }
        return response()->json($produit, 200);
    }

    /**
     *
     *
     * @param int $id
     * @param Request $request
     * @param $totalMontant
     * @return IlluminateHttpResponse
     */
    public function venteProduitParClientTotal($id, Request $request)
    {
        $totalproduitMontant = [];
        $totalMontant = 0;
        $vente = Vente::where('client_commande_id','=',$id)->get();
        foreach ($vente as $vente){
            $produitID = $vente->produit_id;
            $produitVendu_parClient = Produit::find($produitID);
            $produitVendu_pu = $produitVendu_parClient->prix_unitaire;
            $produitVendu_parClient['quantite'] = $vente->quantite;
            $produitVendu_parClient['montant'] = ($vente->quantite * $produitVendu_pu);
            $totalMontant += ($vente->quantite * $produitVendu_pu);
        }
        $totalproduitMontant['montantTotal'] = $totalMontant;
        return response()->json($totalproduitMontant, 200);
    }



}
