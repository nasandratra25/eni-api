<?php

namespace App\Http\Controllers\Commande;

use App\Http\Controllers\Controller;
use App\Models\Commande\Approvisionnement;
use App\Models\Commande\Fournisseur;
use App\Models\Commande\Produit;
use App\Models\Commande\Vente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FournisseurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fournisseurs = Fournisseur::get();
        return response()->json($fournisseurs, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(),[
            'numero_fournisseur'=>['required','unique:fournisseurs'],
            'nom'=>['required'],
            'prenom'=>['required'],
            'adresse'=>['required'],
            'contact'=>['required', 'min:10'],
            'cin'=>['required', 'min:13', 'max:13'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }
        $nom = $request->get('nom');
        $prenom = $request->get('prenom');
        $fournisseur_exist = Fournisseur::where('nom', '=', $nom)->where('prenom', '=', $prenom)->first();
        if (is_null($fournisseur_exist)){
            $fournisseur = new Fournisseur();
            $fournisseur->numero_fournisseur = $request->get('numero_fournisseur');
            $fournisseur->nom = $request->get('nom');
            $fournisseur->prenom = $request->get('prenom');
            $fournisseur->adresse = $request->get('adresse');
            $fournisseur->contact = $request->get('contact');
            $fournisseur->cin = $request->get('cin');
            $fournisseur->save();

            $fournisseurs = Fournisseur::get();
            return response()->json($fournisseurs,201);
        }
        return response()->json(['Erreur'=>'Le Fournisseur existe déjà'],400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fournisseur = Fournisseur::find($id);
        if (!$fournisseur)
        {
            return response()->json(['Erreur'=>'Le Fournisseur n\'est pas disponible'], 204);
        }
        return response()->json($fournisseur, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'numero_fournisseur'=>['required'],
            'nom'=>['required'],
            'prenom'=>['required'],
            'adresse'=>['required'],
            'contact'=>['required', 'min:10'],
            'cin'=>['required', 'min:13', 'max:13'],
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }
        $fournisseur = Fournisseur::find($id);

        if (!$fournisseur)
        {
            return response()->json(['Erreur'=>'Le Fournisseur n\'est pas disponible'], 204);
        }

        $fournisseur->numero_fournisseur = $request->get('numero_fournisseur');
        $fournisseur->nom = $request->get('nom');
        $fournisseur->prenom = $request->get('prenom');
        $fournisseur->adresse = $request->get('adresse');
        $fournisseur->contact = $request->get('contact');
        $fournisseur->cin = $request->get('cin');

        if ($fournisseur->isDirty(['numero_fournisseur'])){
            $num_frs_exist = Fournisseur::where('numero_fournisseur', '=', $request->get('numero_fournisseur'))->first();
            if (!is_null($num_frs_exist)){
                return response()->json(['Erreur'=>'Ce numero existe deja'], 400);
            }
            $fournisseur->save();
            $fournisseurs = Fournisseur::get();
            return response()->json($fournisseurs,200);
        }
        $fournisseur->save();
        $fournisseurs = Fournisseur::get();
        return response()->json($fournisseurs,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fournisseur = Fournisseur::find($id);
        if (!$fournisseur)
        {
            return response()->json(['Erreur'=>'Le Fournisseur n\'est pas disponible'], 204);
        }
        $approFrs = Approvisionnement::where('fournisseur_id', '=', $id)->get();
        foreach ($approFrs as $frs){
            $produitID = $frs->produit_id;
            $ancienstockOld = Produit::find($produitID)->stock;
            $noveaustockOld = floatval($ancienstockOld) - floatval($frs->quantite);
            $stockProduit = Produit::where('id','=',$produitID)
                ->update([
                    'stock' => $noveaustockOld
                ]);
            $frs->delete();
        }
        $fournisseur->delete();
        $fournisseurs = Fournisseur::get();
        return response()->json($fournisseurs,200);
    }

    /**
     *
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function approProduitParFournisseur($id, Request $request)
    {
        $produit = [];
        $appro = Approvisionnement::where('fournisseur_id','=',$id)->get();
        foreach ($appro as $a){
            $produitID = $a->produit_id;
            $produitAppro_parFournisseur = Produit::find($produitID);
            $produitAppro_pu = $produitAppro_parFournisseur->prix_unitaire;
            $produitAppro_parFournisseur['quantite'] = $a->quantite;
            $produitAppro_parFournisseur['montant'] = ($a->quantite * $produitAppro_pu);
            array_push($produit,$produitAppro_parFournisseur);

        }
        return response()->json($produit, 200);
    }
    /**
     *
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function approProduitParFournisseurTotal($id, Request $request)
    {
        $totalproduitMontant = [];
        $totalMontant = 0;
        $appro = Approvisionnement::where('fournisseur_id','=',$id)->get();
        foreach ($appro as $a){
            $produitID = $a->produit_id;
            $produitAppro_parFournisseur = Produit::find($produitID);
            $produitAppro_pu = $produitAppro_parFournisseur->prix_unitaire;
            $produitAppro_parFournisseur['quantite'] = $a->quantite;
            $produitAppro_parFournisseur['montant'] = ($a->quantite * $produitAppro_pu);
            $totalMontant += ($a->quantite * $produitAppro_pu);
        }
        $totalproduitMontant['montantTotal'] = $totalMontant;
        return response()->json($totalproduitMontant, 200);
    }

}
