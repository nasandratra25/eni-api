<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Employee\Employe;
use App\Models\Employee\Entreprise;
use App\Models\Employee\Travail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EntrepriseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return IlluminateHttpResponse
     */
    public function index()
    {
        $entreprises = Entreprise::get();
        return response()->json($entreprises, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return IlluminateHttpResponse
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(),[
            'numero_entreprise'=>['required', 'unique:entreprises'],
            'nom'=>['required'],
            'raison_sociale'=>['required'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }
        $nom = $request->get('nom');
        $entreprise_exist = Entreprise::where('nom', '=', $nom)->first();
        if (is_null($entreprise_exist)){
            $entreprise = new Entreprise();
            $entreprise->numero_entreprise = $request->get('numero_entreprise');
            $entreprise->nom = $request->get('nom');
            $entreprise->raison_sociale = $request->get('raison_sociale');
            $entreprise->save();

            $entreprises = Entreprise::get();
            return response()->json($entreprises,201);
        }
        return response()->json(['Erreur'=>'Entreprise existe déjà'],400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {
        $entreprise = Entreprise::find($id);
        if (!$entreprise)
        {
            return response()->json(['Erreur'=>'Entreprise non disponnible'], 204);
        }
        return response()->json($entreprise, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'numero_entreprise'=>['required'],
            'nom'=>['required'],
            'raison_sociale'=>['required'],
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }

        $entreprise = Entreprise::find($id);
        if (!$entreprise)
        {
            return response()->json(['Entreprise non disponnible'], 204);
        }

        $entreprise->numero_entreprise = $request->get('numero_entreprise');
        $entreprise->nom = $request->get('nom');
        $entreprise->raison_sociale = $request->get('raison_sociale');

        if ($entreprise->isDirty(['numero_entreprise'])){
            $numero_entreprise_exist = Entreprise::where('numero_entreprise', '=', $request->get('numero_entreprise'))->first();
            if (!is_null($numero_entreprise_exist)){
                return response()->json(['Erreur'=>'Ce numero existe deja'], 400);
            }
            $entreprise->save();
            $entreprises = Entreprise::get();
            return response()->json($entreprises,200);
        }
        $entreprise->save();
        $entreprises = Entreprise::get();
        return response()->json($entreprises,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        $entreprise = Entreprise::find($id);
        if (!$entreprise)
        {
            return response()->json(['Erreur'=>'Entreprise non disponnible'], 204);
        }

        $employsParEntreprise = Travail::where('entreprise_id','=', $id)->get();
        foreach ($employsParEntreprise as $employe){
            $employeID = $employe->employe_id;
            $entrepriseID = $employe->entreprise_id;
            $employee = Employe::find($employeID);
            $disponibiliteEmploye = Employe::where('id','=', $employeID)
                ->update([
                    'disponibilite' => 'Disponible'
                ]);
            $travail = Travail::where('entreprise_id', '=', $entrepriseID)
                ->where('employe_id', '=', $employeID)
                ->delete();
            //$travail->delete();
        }
        $entreprise->delete();
        $entreprises = Entreprise::get();
        return response()->json($entreprises,200);
    }
    /**
     *
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function listeEmploye($id)
    {
        $employsParEntreprise = Travail::where('entreprise_id','=', $id)->get();
        $employees = [];
        foreach ($employsParEntreprise as $employe){
            $employeID = $employe->employe_id;
            $employee = Employe::find($employeID);
            $employeTravail = Travail::where('employe_id','=', $employeID)->get();
            $salaireEmploye = (float)($employee->taux_horaire) * (float)($employeTravail[0]['nombre_heure']);
            $employee['salaire'] = $salaireEmploye;
            $employee['nombre_heure'] = $employeTravail[0]['nombre_heure'];
            array_push($employees,$employee);
        }
        return response()->json($employees,200);
    }
    /**
     *
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function nombreEmployes($id)
    {
        $nb_employsParEntreprise = Travail::where('entreprise_id','=', $id)->count();

        return response()->json(['nombre_employe'=>$nb_employsParEntreprise],200);
    }

}
