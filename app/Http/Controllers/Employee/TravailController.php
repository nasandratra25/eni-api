<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Employee\Employe;
use App\Models\Employee\Travail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TravailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return IlluminateHttpResponse
     */
    public function index()
    {
        $travails = Travail::get();
        return response()->json($travails, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return IlluminateHttpResponse
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(),[
            'employe_id'=>['required','numeric'],
            'entreprise_id'=>['required','numeric'],
            'nombre_heure'=>['required','numeric'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }
        $employe_id = $request->get('employe_id');
        $employe_non_dispo = Employe::where('id', '=', $employe_id)->first();
        if ($employe_non_dispo->disponibilite == 'Disponible'){
            $travail = new Travail();
            $travail->employe_id = $request->get('employe_id');
            $travail->entreprise_id = $request->get('entreprise_id');
            $travail->nombre_heure = $request->get('nombre_heure');
            $disponibiliteEmploye = Employe::where('id','=', $request->get('employe_id'))
                ->update([
                    'disponibilite' => 'Non disponible'
                ]);
            $travail->save();
            $travails = Travail::get();
            return response()->json($travails,201);
        }
        return response()->json(['Erreur'=>'Employe travail déjà'],400);

       // return response()->json($employe_non_dispo->disponibilite,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {
        $travail = Travail::find($id);
        if (!$travail)
        {
            return response()->json(['Erreur'=>'Travail non disponnible'], 204);
        }
        return response()->json($travail, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'employe_id'=>['required','numeric'],
            'entreprise_id'=>['required','numeric'],
            'nombre_heure'=>['required','numeric'],
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }

        $travail = Travail::find($id);
        if (!$travail)
        {
            return response()->json(['Travail non disponnible'], 204);
        }

        $travail->employe_id = $request->get('employe_id');
        $travail->entreprise_id = $request->get('entreprise_id');
        $travail->nombre_heure = $request->get('nombre_heure');

        if ($travail->isDirty(['employe_id'])){
            $employe_non_dispo = Employe::where('id', '=', $request->get('employe_id'))->first();
            if ($employe_non_dispo->disponibilite == 'Disponible'){
                $travailOld = Travail::find($id);
                $ancienEmployeId = $travailOld->employe_id;
                $disponibiliteEmployeOld = Employe::where('id','=', $ancienEmployeId)
                    ->update([
                        'disponibilite' => 'Disponible'
                    ]);

                $disponibiliteEmploye = Employe::where('id','=', $request->get('employe_id'))
                    ->update([
                        'disponibilite' => 'Non disponible'
                    ]);
                $travail->save();
                $travails = Travail::get();
                return response()->json($travails,200);
            }
            return response()->json(['Erreur'=>'Employe Travail deja'], 400);

        }
        $travail->save();
        $travails = Travail::get();
        return response()->json($travails,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        $travail = Travail::find($id);
        if (!$travail)
        {
            return response()->json(['Erreur'=>'Travail non disponnible'], 204);
        }
        $ancienEmployeId = $travail->employe_id;

        $disponibiliteEmploye = Employe::where('id','=', $ancienEmployeId)
            ->update([
                'disponibilite' => 'Disponible'
            ]);
        $travail->delete();
        $travails = Travail::get();
        return response()->json($travails,201);
    }
}
