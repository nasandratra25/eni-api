<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Employee\Employe;
use App\Models\Employee\Entreprise;
use App\Models\Employee\Travail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return IlluminateHttpResponse
     */
    public function index()
    {
        $employes = Employe::get();
        return response()->json($employes, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return IlluminateHttpResponse
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(),[
            'numero_employe'=>['required', 'unique:employes'],
            'nom'=>['required'],
            'prenom'=>['required'],
            'adresse'=>['required'],
            'taux_horaire'=>['required','numeric'],
            'contact'=>['required', 'min:10'],
            'cin'=>['required', 'min:13', 'max:13'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }
        $nom = $request->get('nom');
        $prenom = $request->get('prenom');
        $employe_exist = Employe::where('nom', '=', $nom)->where('prenom', '=', $prenom)->first();
        if (is_null($employe_exist)){
            $employe = new Employe();
            $employe->numero_employe = $request->get('numero_employe');
            $employe->nom = $request->get('nom');
            $employe->prenom = $request->get('prenom');
            $employe->adresse = $request->get('adresse');
            $employe->contact = $request->get('contact');
            $employe->taux_horaire = $request->get('taux_horaire');
            $employe->cin = $request->get('cin');
            $employe->save();

            $employes = Employe::get();
            return response()->json($employes,201);
        }
        return response()->json(['Erreur'=>'L\'employe existe déjà'],400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {
        $employe = Employe::find($id);
        if (!$employe)
        {
            return response()->json(['Erreur'=>'L\'employe n\'est pas disponible'], 204);
        }
        return response()->json($employe, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'numero_employe'=>['required'],
            'nom'=>['required'],
            'prenom'=>['required'],
            'adresse'=>['required'],
            'taux_horaire'=>['required','numeric'],
            'contact'=>['required', 'min:10'],
            'cin'=>['required', 'min:13', 'max:13'],
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }

        $employe = Employe::find($id);
        if (!$employe)
        {
            return response()->json(['L\'employe n\'est pas disponible'], 204);
        }

        $employe->numero_employe = $request->get('numero_employe');
        $employe->nom = $request->get('nom');
        $employe->prenom = $request->get('prenom');
        $employe->adresse = $request->get('adresse');
        $employe->contact = $request->get('contact');
        $employe->taux_horaire = $request->get('taux_horaire');
        $employe->cin = $request->get('cin');

        if ($employe->isDirty(['numero_employe'])){
            $numero_employe_exist = Employe::where('numero_employe', '=', $request->get('numero_employe'))->first();
            if (!is_null($numero_employe_exist)){
                return response()->json(['Erreur'=>'Ce numero existe deja'], 400);
            }
            $employe->save();
            $employes = Employe::get();
            return response()->json($employes,200);
        }
        $employe->save();
        $employes = Employe::get();
        return response()->json($employes,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        $employe = Employe::find($id);
        if (!$employe)
        {
            return response()->json(['Erreur'=>'L\'employe n\'est pas disponible'], 204);
        }
        $employsParTravail = Travail::where('employe_id','=', $id)->delete();
        $employe->delete();
        $employes = Employe::get();
        return response()->json($employes,200);
    }

    /**
     *
     * @return IlluminateHttpResponse
     */
    public function salaireGlobauxEmploye()
    {
        $tousEmployes = Employe::get();
        $employees = [];
        foreach ($tousEmployes as $employes){
            $employeID = $employes->id;
            $employeTravail = Travail::where('employe_id','=', $employeID)->first();
            if($employeTravail != null){
                $salaireEmploye = (float)($employes->taux_horaire) * (float)($employeTravail['nombre_heure']);
                $employes['salaire'] = $salaireEmploye;
                $employes['nombre_heure'] = $employeTravail['nombre_heure'];
                array_push($employees,$employes);
            }else {
                $employes['salaire'] = 0;
                $employes['nombre_heure'] = 0;
                array_push($employees,$employes);
            }
        }
        return response()->json($employees,200);
    }
    /**
     *
     * @return IlluminateHttpResponse
     */
    public function montantTotalEmploye()
    {
        $tousEmployes = Employe::get();
        $salaireTotal = 0;
        foreach ($tousEmployes as $employes){
            $employeID = $employes->id;
            $employeTravail = Travail::where('employe_id','=', $employeID)->first();
            if ($employeTravail != null){
                $salaireEmploye = (float)($employes->taux_horaire) * (float)($employeTravail['nombre_heure']);
                $salaireTotal += $salaireEmploye;
            }
            $salaireTotal += 0;
        }
        return response()->json(['salaire_total'=>$salaireTotal],200);
    }
    /**
     *
     * @return IlluminateHttpResponse
     */
    public function employeDisponible()
    {
        $employe_dispo = Employe::where('disponibilite', '=', 'Disponible')->get();
        return response()->json($employe_dispo,200);
    }


}
