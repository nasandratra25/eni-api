<?php

namespace App\Http\Controllers\Bancaire;

use App\Http\Controllers\Controller;
use App\Models\Bancaire\ClientBanque;
use App\Models\Bancaire\Retrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RetraitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $retraits = Retrait::get();
        return response()->json($retraits, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(),[
            'numero_retrait'=>['required','unique:retraits'],
            'client_banque_id'=>['required'],
            'montant'=>['required','numeric', 'min:1'],
            'numero_cheque'=>['required','numeric','unique:retraits'],
            'date_retrait'=>['required','date'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }
        $retrait = new Retrait();
        $retrait->numero_retrait = $request->get('numero_retrait');
        $retrait->client_banque_id = $request->get('client_banque_id');
        $retrait->montant = $request->get('montant');
        $retrait->numero_cheque = $request->get('numero_cheque');
        $retrait->date_retrait = $request->get('date_retrait');

        $ancienSolde = ClientBanque::find($request->get('client_banque_id'))->solde;
        if (!(floatval($ancienSolde) < floatval($request->get('montant')))){
            $noveauSolde = floatval($ancienSolde) - floatval($request->get('montant'));
            $soldClient = ClientBanque::where('id','=',$request->get('client_banque_id'))
                ->update([
                    'solde' => $noveauSolde
                ]);
            $retrait->save();
            $retraits = Retrait::get();
            return response()->json($retraits,201);
        }
        return response()->json(['Attention' => 'Le solde es Insuffisant'], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $retrait = Retrait::find($id);
        if (!$retrait) {
            return response()->json(['Erreur' => 'Retrait non disponible'], 204);
        }
        return response()->json($retrait,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'numero_retrait'=>['required'],
            'client_banque_id'=>['required'],
            'montant'=>['required','numeric', 'min:1'],
            'numero_cheque'=>['required','numeric'],
            'date_retrait'=>['required','date'],
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }
        $retrait = Retrait::find($id);
        if (!$retrait)
        {
            return response()->json(['Erreur'=>'Retrait non disponible'], 204);
        }
        $anciensoldeRetrait = $retrait->montant;
        $retrait->numero_retrait = $request->get('numero_retrait');
        $retrait->client_banque_id = $request->get('client_banque_id');
        $retrait->montant = $request->get('montant');
        $retrait->numero_cheque = $request->get('numero_cheque');
        $retrait->date_retrait = $request->get('date_retrait');

        if ($retrait->isDirty(['numero_retrait'])){
            $numero_retrait_exist = Retrait::where('numero_retrait', '=', $request->get('numero_retrait'))->first();
            if (!is_null($numero_retrait_exist)){
                return response()->json(['Erreur'=>'Ce numero retrait existe deja'], 400);
            }
        }

        if ($retrait->isDirty(['client_banque_id'])){
            $retraitOld = Retrait::find($id);
            $ancienClient = $retraitOld->client_banque_id;
            $ancienSolde = ClientBanque::find($ancienClient)->solde;
            $noveauSolde = floatval($ancienSolde) + floatval($anciensoldeRetrait);
            $soldClient = ClientBanque::where('id','=',$ancienClient)
                ->update([
                    'solde' => $noveauSolde
                ]);
            $ancienSolde = ClientBanque::find($request->get('client_banque_id'))->solde;
            if (!(floatval($ancienSolde) < floatval($request->get('montant')))) {
                $noveauSolde = floatval($ancienSolde) - floatval($request->get('montant'));
                $soldClient = ClientBanque::where('id','=',$request->get('client_banque_id'))
                    ->update([
                        'solde' => $noveauSolde
                    ]);
                $retrait->save();
                $retraits = Retrait::get();
                return response()->json($retraits,200);
            }
            return response()->json(['Attention' => 'Le solde es Insuffisant'], 400);
        }

        $ancienSolde = ClientBanque::find($request->get('client_banque_id'))->solde;
        $ancienSolde = floatval($ancienSolde) + floatval($anciensoldeRetrait);
        if (!(floatval($ancienSolde) < floatval($request->get('montant')))) {
            $noveauSolde = floatval($ancienSolde) - floatval($request->get('montant'));
            $soldClient = ClientBanque::where('id','=',$request->get('client_banque_id'))
                ->update([
                    'solde' => $noveauSolde
                ]);
            $retrait->save();
            $retraits = Retrait::get();
            return response()->json($retraits,200);
        }
        return response()->json(['Attention' => 'Le solde es Insuffisant'], 400);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $retrait = Retrait::find($id);
        if (!$retrait)
        {
            return response()->json(['Erreur'=>'Retrait non disponible'], 204);
        }
        $anciensoldeVersement = $retrait->montant;
        $ancienClient = $retrait->client_banque_id;
        $ancienSolde = ClientBanque::find($ancienClient)->solde;
        $noveauSolde = floatval($ancienSolde) + floatval($anciensoldeVersement);
        $soldClient = ClientBanque::where('id','=',$ancienClient)
            ->update([
                'solde' => $noveauSolde
            ]);
        $retrait->delete();
        $retraits = Retrait::get();
        return response()->json($retraits,200);
    }
}
