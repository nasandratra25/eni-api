<?php

namespace App\Http\Controllers\Bancaire;

use App\Http\Controllers\Controller;
use App\Models\Bancaire\ClientBanque;
use App\Models\Bancaire\Retrait;
use App\Models\Bancaire\Versement;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $clients = ClientBanque::get();
        return response()->json($clients, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(),[
            'nom'=>['required'],
            'prenom'=>['required'],
            'adresse'=>['required'],
            'contact'=>['required', 'min:10'],
            'cin'=>['required', 'min:13', 'max:13'],
            'numero_compte'=>['required', 'numeric','unique:client_banques'],
            'solde'=>[ 'numeric'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }
        $nom = $request->get('nom');
        $prenom = $request->get('prenom');
        $client_exist = ClientBanque::where('nom', '=', $nom)->where('prenom', '=', $prenom)->first();
        if (is_null($client_exist)){
            $client = new ClientBanque();
            $client->nom = $request->get('nom');
            $client->prenom = $request->get('prenom');
            $client->adresse = $request->get('adresse');
            $client->contact = $request->get('contact');
            $client->cin = $request->get('cin');
            $client->numero_compte = $request->get('numero_compte');
            if (!is_null($request->get('solde'))){
                $client->solde = $request->get('solde');
            }

            $client->save();
            $clients = ClientBanque::get();
            return response()->json($clients,201);
        }
        return response()->json(['Erreur'=>'le client existe déjà'],400);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $client = ClientBanque::find($id);
        if (!$client)
        {
            return response()->json(['Erreur'=>'Le client n\'est pas disponible'], 204);
        }
        return response()->json($client, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'nom'=>['required'],
            'prenom'=>['required'],
            'adresse'=>['required'],
            'contact'=>['required', 'min:10'],
            'cin'=>['required', 'min:13', 'max:13'],
            'numero_compte'=>['required', 'numeric'],
            'solde'=>[ 'numeric'],
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }

        $client = ClientBanque::find($id);
        if (!$client)
        {
            return response()->json(['Erreur'=>'Le client n\'est pas disponible'], 204);
        }
        $client->nom = $request->get('nom');
        $client->prenom = $request->get('prenom');
        $client->adresse = $request->get('adresse');
        $client->contact = $request->get('contact');
        $client->cin = $request->get('cin');
        $client->numero_compte = $request->get('numero_compte');
        if (!is_null($request->get('solde'))){
            $client->solde = $request->get('solde');
        }

        if ($client->isDirty(['numero_compte'])){
            $num_compte_exist = ClientBanque::where('numero_compte', '=', $request->get('numero_compte'))->first();
            if (!is_null($num_compte_exist)){
                return response()->json(['Erreur'=>'Ce numero de compte existe deja'], 400);
            }
            $client->save();
            $clients = ClientBanque::get();
            return response()->json($clients,200);
        }
        $client->save();
        $clients = ClientBanque::get();
        return response()->json($clients,201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $client = ClientBanque::find($id);
        if (!$client)
        {
            return response()->json(['Erreur'=>'Le client n\'est pas disponible'], 204);
        }
        $versementCli = Versement::where('client_banque_id', '=', $id)->delete();
        $retraitCli = Retrait::where('client_banque_id', '=', $id)->delete();
        $client->delete();
        $clients = ClientBanque::get();
        return response()->json($clients,200);
    }

    /**
     * Search the specified resource from storage.
     *
     *
     * @return Response
     */
    public function rechercheClient(Request $request)
    {
        $motRechercher = $request->get('mot_recherche');
        $clients = ClientBanque::where(function($query) use ($motRechercher){
            $query->where('numero_compte', 'LIKE', '%'.$motRechercher.'%');
            $query->orWhere('nom', 'LIKE', '%'.$motRechercher.'%');
            $query->orWhere('prenom', 'LIKE', '%'.$motRechercher.'%');
            $query->orWhere('adresse', 'LIKE', '%'.$motRechercher.'%');
        })->get();
        return response()->json($clients,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     *
     * @return Response
     */
    public function effectifClient()
    {
        $clients = ClientBanque::get();
        $clientsEffectif = $clients->count();
        return response()->json(['effectif'=>$clientsEffectif],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function versementClient($id)
    {
        $clients = ClientBanque::find($id);
        $versement = $clients->versements;
        return response()->json($versement,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function retraitClient($id)
    {
        $clients = ClientBanque::find($id);
        $retrait = $clients->retraits;
        return response()->json($retrait,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function totalTransactionClient($id)
    {
        $clients = ClientBanque::find($id);
        $retraitTotal = $clients->retraits->sum('montant');
        $versementTotal = $clients->versements->sum('montant');
        return response()->json([
            'totalRetraits'=> $retraitTotal,
            'totalVersement'=> $versementTotal,
        ],200);
    }
}
