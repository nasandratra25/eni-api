<?php

namespace App\Http\Controllers\Bancaire;

use App\Http\Controllers\Controller;
use App\Models\Bancaire\ClientBanque;
use App\Models\Bancaire\Versement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VersementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $versements = Versement::get();
        return response()->json($versements, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createValidation = Validator::make($request->all(),[
            'numero_versement'=>['required','unique:versements'],
            'client_banque_id'=>['required'],
            'montant'=>['required','numeric', 'min:1'],
            'date_versement'=>['required','date'],
        ]);
        if ($createValidation->fails()){
            return response()->json($createValidation->errors(),400);
        }
        $versement = new Versement();
        $versement->numero_versement = $request->get('numero_versement');
        $versement->client_banque_id = $request->get('client_banque_id');
        $versement->montant = $request->get('montant');
        $versement->date_versement = $request->get('date_versement');

        $ancienSolde = ClientBanque::find($request->get('client_banque_id'))->solde;
        $noveauSolde = floatval($ancienSolde) + floatval($request->get('montant'));
        $soldClient = ClientBanque::where('id','=',$request->get('client_banque_id'))
            ->update([
                'solde' => $noveauSolde
            ]);
        $versement->save();
        $versements = Versement::get();
        return response()->json($versements,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $versement = Versement::find($id);
        if (!$versement)
        {
            return response()->json(['Erreur'=>'Versement non disponible'], 204);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateValidation = Validator::make($request->all(),[
            'numero_versement'=>['required'],
            'client_banque_id'=>['required'],
            'montant'=>['required','numeric', 'min:1'],
            'date_versement'=>['required','date'],
        ]);
        if ($updateValidation->fails()){
            return response()->json($updateValidation->errors(),400);
        }
        $versement = Versement::find($id);
        if (!$versement)
        {
            return response()->json(['Erreur'=>'Versement non disponible'], 204);
        }
        $anciensoldeVersement = $versement->montant;
        $versement->numero_versement = $request->get('numero_versement');
        $versement->client_banque_id = $request->get('client_banque_id');
        $versement->montant = $request->get('montant');
        $versement->date_versement = $request->get('date_versement');

        if ($versement->isDirty(['numero_versement'])){
            $numero_versement_exist = Versement::where('numero_versement', '=', $request->get('numero_versement'))->first();
            if (!is_null($numero_versement_exist)){
                return response()->json(['Erreur'=>'Ce numero versement existe deja'], 400);
            }
        }

        if ($versement->isDirty(['client_banque_id'])){
            $versementOld = Versement::find($id);
            $ancienClient = $versementOld->client_banque_id;
            $ancienSolde = ClientBanque::find($ancienClient)->solde;
            $noveauSolde = floatval($ancienSolde) - floatval($anciensoldeVersement);
            $soldClient = ClientBanque::where('id','=',$ancienClient)
                ->update([
                    'solde' => $noveauSolde
                ]);
            $ancienSolde = ClientBanque::find($request->get('client_banque_id'))->solde;
            $noveauSolde = floatval($ancienSolde) + floatval($request->get('montant'));
            $soldClient = ClientBanque::where('id','=',$request->get('client_banque_id'))
                ->update([
                    'solde' => $noveauSolde
                ]);
            $versement->save();
            $versements = Versement::get();
            return response()->json($versements,200);
        }

            $ancienSolde = ClientBanque::find($request->get('client_banque_id'))->solde;
            $noveauSolde = floatval($ancienSolde) + floatval($request->get('montant')) - floatval($anciensoldeVersement);
            $soldClient = ClientBanque::where('id','=',$request->get('client_banque_id'))
                ->update([
                    'solde' => $noveauSolde
                ]);
        $versement->save();
        $versements = Versement::get();
        return response()->json($versements,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $versement = Versement::find($id);
        if (!$versement)
        {
            return response()->json(['Erreur'=>'Versement non disponible'], 204);
        }
        $anciensoldeVersement = $versement->montant;
        $ancienClientID = $versement->client_banque_id;
        $clients = ClientBanque::find($ancienClientID);
        $retraitTotal = (float)$clients->retraits->sum('montant');
        $versementTotal = (float)$clients->versements->sum('montant');
        if (!($retraitTotal > $versementTotal)) {
            $ancienSolde = ClientBanque::find($ancienClientID)->solde;
            $noveauSolde = floatval($ancienSolde) - floatval($anciensoldeVersement);
            $soldClient = ClientBanque::where('id','=',$ancienClientID)
                ->update([
                    'solde' => $noveauSolde
                ]);
            $versement->delete();
            $versements = Versement::get();
            return response()->json($versements,200);
        }
        return response()->json(['Attention' => 'Le stock est Insuffisant'], 400);

    }
}
